/*
 * GraphicsComponent.h
 *
 *  Created on: 26 de mar de 2016
 *      Author: cassiano
 */

#ifndef SRC_GRAPHICSCOMPONENT_H_
#define SRC_GRAPHICSCOMPONENT_H_

#include "Component.h"
#include "Entity.h"

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

namespace components {

class GraphicsComponent: public Component,
		public sf::Drawable,
		public sf::Transformable {
public:
	GraphicsComponent(sf::RenderWindow& window, const sf::Sprite& sprite);
	virtual ~GraphicsComponent();

	void update(Entity* entity, const sf::Time& dt) override;

private:
	void draw(sf::RenderTarget& target,
			sf::RenderStates states) const override;

	sf::RenderWindow& mWindow;
	sf::Sprite mSprite;
};

} /* namespace components */

#endif /* SRC_GRAPHICSCOMPONENT_H_ */
