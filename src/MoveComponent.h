/*
 * MoveComponent.h
 *
 *  Created on: 26 de mar de 2016
 *      Author: cassiano
 */

#ifndef SRC_MOVECOMPONENT_H_
#define SRC_MOVECOMPONENT_H_

#include "Component.h"

namespace components {

class MoveComponent : public Component {
public:
	MoveComponent(float velocity);
	virtual ~MoveComponent();

	void resetDirection();

	void setDirection(const sf::Vector2f direction) { mDirection = direction; }
	const sf::Vector2f& getDirection() const { return mDirection; }

	void update(Entity* entity, const sf::Time& dt) override;

private:
	float mVelocity;
	sf::Vector2f mDirection;
};

} /* namespace components */

#endif /* SRC_MOVECOMPONENT_H_ */
