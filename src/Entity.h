/*
 * Entity.h
 *
 *  Created on: 26 de mar de 2016
 *      Author: cassiano
 */

#ifndef SRC_ENTITY_H_
#define SRC_ENTITY_H_

#include <map>
#include <typeinfo>
#include <typeindex>
#include <assert.h>
#include <type_traits>
#include <SFML/Graphics.hpp>

#include "Component.h"

namespace components {

class Entity {
public:
	Entity() {}
	virtual ~Entity() {}

	template<typename T> void addComponent(T* component) {
		//assert(dynamic_cast<Component*>(component));
		//assert(std::is_base_of<Component, T>::value == false);
		static_assert( std::is_base_of<Component, T>::value, "Invalid component to add\n");

		mComponents.insert(
				std::pair<std::type_index, Component*>(typeid(T), component));
	}

	template<typename T> T* getComponent() {
		static_assert( std::is_base_of<Component, T>::value, "Invalid component to get\n");
		auto c = mComponents.find(typeid(T));

		if (c != mComponents.end()) {
			return dynamic_cast<T*>(c->second);
		}

		return nullptr;
	}

	void update(const sf::Time& dt) {
		updateComponents(dt);
	}

private:
	void updateComponents(const sf::Time& dt) {
		for (auto c : mComponents) {
			c.second->update(this, dt);
		}
	}

	std::map<std::type_index, Component*> mComponents;
};

//template<typename T> T* Entity::getComponent() {
//	auto c = mComponents.find(typeid(T));
//
//	if (c != mComponents.end()) {
//		return dynamic_cast<T*>(*c);
//	}
//
//	return nullptr;
//}

} /* namespace components */

#endif /* SRC_ENTITY_H_ */
