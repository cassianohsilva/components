/*
 * Component.h
 *
 *  Created on: 26 de mar de 2016
 *      Author: cassiano
 */

#ifndef SRC_COMPONENT_H_
#define SRC_COMPONENT_H_

#include <SFML/System.hpp>

namespace components {

class Component {
public:
	virtual ~Component() {}

	virtual void update(class Entity* entity, const sf::Time& dt) = 0;
};

} /* namespace components */

#endif /* SRC_COMPONENT_H_ */
