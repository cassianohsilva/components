/*
 * MoveComponent.cpp
 *
 *  Created on: 26 de mar de 2016
 *      Author: cassiano
 */

#include "MoveComponent.h"

#include <math.h>

#include "Entity.h"
#include "GraphicsComponent.h"

namespace components {

MoveComponent::MoveComponent(float velocity) : mVelocity(velocity) {

}

MoveComponent::~MoveComponent() {

}

void MoveComponent::resetDirection() {
	mDirection.x = 0.0f;
	mDirection.y = 0.0f;
}

void MoveComponent::update(Entity* entity, const sf::Time& dt) {
	GraphicsComponent* graphics = entity->getComponent<GraphicsComponent>();

	if(graphics) {
		sf::Vector2f lastPosition = graphics->getPosition();

		sf::Vector2f normalized = mDirection / sqrtf( powf(mDirection.x, 2.0f) + (mDirection.y, 2.0f) );

		graphics->setPosition(lastPosition + (mVelocity * dt.asSeconds() * normalized));
	}
}

} /* namespace components */
