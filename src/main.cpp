//============================================================================
// Name        : Components.cpp
// Author      : Cassiano Honorio da Silva
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C, Ansi-style
//============================================================================

#include <stdio.h>
#include <stdlib.h>

#include "GraphicsComponent.h"
#include "MoveComponent.h"
#include "Player.h"

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

using namespace components;

int main(void) {

	sf::RenderWindow window(sf::VideoMode(800, 600), "Components");

	Player* p = new Player();

	sf::Texture texture;
	sf::Sprite sprite;
	if (texture.loadFromFile("res/Simpsons.png")) {
		sprite.setTexture(texture);
	}

	GraphicsComponent* graphics = new GraphicsComponent(window, sprite);
	MoveComponent* movement = new MoveComponent(50.0f);

	movement->setDirection(sf::Vector2f(1.0f, 0.0f));

	p->addComponent(graphics);
	p->addComponent(movement);

	sf::Clock clock;

	while (window.isOpen()) {
		sf::Event e;

		if (window.pollEvent(e)) {
			if (e.type == sf::Event::Closed) {
				window.close();
			}
		}

		sf::Time elapsedTime = clock.getElapsedTime();
		clock.restart();

		window.clear(sf::Color::White);
		p->update(elapsedTime);
		window.display();
	}

	return EXIT_SUCCESS;
}
