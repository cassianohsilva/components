/*
 * GraphicsComponent.cpp
 *
 *  Created on: 26 de mar de 2016
 *      Author: cassiano
 */

#include "GraphicsComponent.h"

namespace components {

GraphicsComponent::GraphicsComponent(sf::RenderWindow& window,
		const sf::Sprite& sprite) :
		mWindow(window), mSprite(sprite) {

}

GraphicsComponent::~GraphicsComponent() {

}

void GraphicsComponent::update(Entity* entity, const sf::Time& dt) {
	mWindow.draw(*this);
}

void GraphicsComponent::draw(sf::RenderTarget& target,
		sf::RenderStates states) const {
	states.transform = getTransform();

	mWindow.draw(mSprite, states);
}

} /* namespace components */
