/*
 * Player.h
 *
 *  Created on: 26 de mar de 2016
 *      Author: cassiano
 */

#ifndef SRC_PLAYER_H_
#define SRC_PLAYER_H_

#include "Entity.h"

namespace components {

class Player : public Entity {
public:
	Player();
	virtual ~Player();
};

} /* namespace components */

#endif /* SRC_PLAYER_H_ */
